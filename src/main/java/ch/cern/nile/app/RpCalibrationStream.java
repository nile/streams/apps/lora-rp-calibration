package ch.cern.nile.app;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ch.cern.nile.app.generated.RpCalibrationPacket;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;

public final class RpCalibrationStream extends LoraDecoderStream<RpCalibrationPacket> {

    private static final Map<String, String> CUSTOM_METADATA_FIELD_MAP = new HashMap<>();

    static {
        CUSTOM_METADATA_FIELD_MAP.put("deviceName", "device_name");
        CUSTOM_METADATA_FIELD_MAP.put("fPort", "fPort");
    }

    public RpCalibrationStream() {
        super(RpCalibrationPacket.class, CUSTOM_METADATA_FIELD_MAP, false, false, false);
    }

    @Override
    public Map<String, Object> enrichCustomFunction(final Map<String, Object> map, final JsonObject value) {
        setRxInfo(map, (JsonArray) value.get("rxInfo"));
        return map;
    }

    private static void setRxInfo(final Map<String, Object> output, final JsonArray rxInfo) {
        int index = 0;
        for (final JsonElement element : rxInfo) {
            final JsonObject entry = element.getAsJsonObject();
            addGatewayProperty(entry, "rssi", "rssi_gw_%d", index, output);
            addGatewayProperty(entry, "loRaSNR", "snr_gw_%d", index, output);
            index++;
        }
    }

    private static void addGatewayProperty(final JsonObject entry, final String propertyName,
                                           final String formattedName, final int index,
                                           final Map<String, Object> output) {
        if (entry.has(propertyName)) {
            output.put(String.format(formattedName, index), entry.get(propertyName).getAsInt());
        }
    }


}
